#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>


#include "CsuAnalysisCode2019/ZprimeAnalysis.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  std::string baseDir = "";
  //std::string sample = "submitDir";
  //if( argc > 1 ) submitDir = argv[ 1 ];
  if( argc > 1 ) baseDir = argv[ 1 ];

  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  //Loop over all the samples
  std::vector<std::string> samples;
  //Zprime
  // samples.push_back("ZPrime400");  samples.push_back("ZPrime500");  
  samples.push_back("ZPrime750"); 
  // samples.push_back("ZPrime1000");
  // samples.push_back("ZPrime1250");  samples.push_back("ZPrime1500");  samples.push_back("ZPrime1750");  samples.push_back("ZPrime2000");
  // samples.push_back("ZPrime2250");  
  samples.push_back("ZPrime2500"); 
  // samples.push_back("ZPrime3000");
  //Diboson
  // samples.push_back("WW"); samples.push_back("WZ");samples.push_back("ZZ");
  //Drell-Yan
  // samples.push_back("DYeeM08to15"); samples.push_back("DYeeM15to40"); samples.push_back("DYmumuM08to15"); 
  // samples.push_back("DYmumuM15to40"); samples.push_back("DYtautauM08to15"); samples.push_back("DYtautauM15to40");  
  //W
  // samples.push_back("WenuJetsBVeto"); samples.push_back("WenuWithB"); samples.push_back("WenuNoJetsBVeto"); samples.push_back("WmunuJetsBVeto"); samples.push_back("WmunuWithB"); 
  // samples.push_back("WmunuNoJetsBVeto"); samples.push_back("WtaunuJetsBVeto");samples.push_back("WtaunuWithB"); samples.push_back("WtaunuNoJetsBVeto");   
  //Z
  // samples.push_back("Zee"); samples.push_back("Zmumu");samples.push_back("Ztautau");
  //single top
  // samples.push_back("stop_tchan_top"); samples.push_back("stop_tchan_antitop");samples.push_back("stop_schan"); samples.push_back("stop_wtchan");
  // ttbar
  // samples.push_back("ttbar_lep"); samples.push_back("ttbar_had");
  // data
  // samples.push_back("DataMuons"); samples.push_back("DataEgamma");

  for (unsigned int i = 0; i < samples.size(); i++){
    // Construct the samples to run on:
    SH::SampleHandler sh;
    
    submitDir = (baseDir+"_"+samples.at(i)).c_str();    

    // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
    //const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/user/p/palacino/ATLAS_OpenData/MC/ZPrime2500");
    //const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/user/p/palacino/ATLAS_OpenData/MC/ttbar_lep");
    //const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/user/p/palacino/ATLAS_OpenData/MC/WW");
    //const char* inputFilePath = gSystem->ExpandPathName ("/eos/atlas/user/p/palacino/ATLAS_OpenData/MC/DYeeM15to40");
    //const char* inputFilePath = gSystem->ExpandPathName (("/eos/atlas/user/p/palacino/ATLAS_OpenData/MC/"+samples.at(i)).c_str());
    //const char* inputFilePath = gSystem->ExpandPathName (("/eos/atlas/user/p/palacino/ATLAS_OpenData/Data/"+samples.at(i)).c_str());
    const char* inputFilePath;
    if(samples.at(i).find("Data") == std::string::npos) inputFilePath = gSystem->ExpandPathName (("/eos/atlas/user/p/palacino/ATLAS_OpenData/MC/"+samples.at(i)).c_str());
    else inputFilePath = gSystem->ExpandPathName (("/eos/atlas/user/p/palacino/ATLAS_OpenData/Data/"+samples.at(i)).c_str());
    SH::ScanDir().filePattern("*root*").scan(sh,inputFilePath);
    
    
    // Set the name of the input TTree.
    sh.setMetaString( "nc_tree", "mini" );
    
    // Print what we found:
    sh.print();
    
    // Create an EventLoop job:
    EL::Job job;
    job.sampleHandler( sh );
    job.options()->setDouble (EL::Job::optMaxEvents, -1);
    
    // Add our analysis to the job:
    ZprimeAnalysis* alg = new ZprimeAnalysis();
    if(samples.at(i).find("Data") == std::string::npos)  alg->setData(false);
    else alg->setData(true);
     
    job.algsAdd( alg );

    
    // Run the job using the local/direct driver:
    EL::DirectDriver driver;
    driver.submit( job, submitDir );
    

  }
  return 0;
}

void PlottingMacro(){
  //Creating Histograms
  TFile *f750 = TFile::Open("hist-Zprime750.root","READ");
  TH1F *h_ZP750_lep_pt = (TH1F*) f750->Get("h_lep_pt");
  TH1F *h_ZP750_lep_eta = (TH1F*) f750->Get("h_lep_eta");
  TH1F *h_ZP750_lep_phi = (TH1F*) f750->Get("h_lep_phi");

  TFile *f2500 = TFile::Open("hist-Zprime2500.root","READ");
  TH1F *h_ZP2500_lep_pt = (TH1F*) f2500->Get("h_lep_pt");
  TH1F *h_ZP2500_lep_eta = (TH1F*) f2500->Get("h_lep_eta");
  TH1F *h_ZP2500_lep_phi = (TH1F*) f2500->Get("h_lep_phi");

  //Scaling each histogram
  h_ZP750_lep_pt->Scale(1./h_ZP750_lep_pt->Integral());
  h_ZP2500_lep_pt->Scale(1./h_ZP2500_lep_pt->Integral());
  
  h_ZP2500_lep_pt->SetLineColor(kRed);
  
  h_ZP750_lep_eta->Scale(1./h_ZP750_lep_eta->Integral());
  h_ZP2500_lep_eta->Scale(1./h_ZP2500_lep_eta->Integral());

  h_ZP2500_lep_eta->SetLineColor(kRed);

  h_ZP750_lep_phi->Scale(1./h_ZP750_lep_phi->Integral());
  h_ZP2500_lep_phi->Scale(1./h_ZP2500_lep_phi->Integral());

  h_ZP2500_lep_phi->SetLineColor(kRed);

  //Creating Canvases
  TCanvas *c_pt = new TCanvas("c_pt","",600,600);
  c_pt->cd();
  h_ZP750_lep_pt->Draw("hist");
  h_ZP2500_lep_pt->Draw("same");

  TCanvas *c_eta = new TCanvas("c_eta","",600,600);
  c_eta->cd();
  h_ZP750_lep_eta->Draw("hist");
  h_ZP2500_lep_eta->Draw("same");

  TCanvas *c_phi = new TCanvas("c_phi","",600,600);
  c_phi->cd();
  h_ZP750_lep_phi->Draw("hist");
  h_ZP2500_lep_phi->Draw("same");

}
